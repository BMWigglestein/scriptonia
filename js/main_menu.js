// Activate the extension bu calling activate.js
function activateScriptonia() {
  chrome.tabs.executeScript({
    file: 'js/activate.js'
  });
}

// Deactivate the extension(This only makes it so the editor looks normal, The 
// extension is not actually 'deactivated')
function deactivateScriptonia() {
  chrome.tabs.executeScript({
    file: 'js/deactivate.js'
  });
}

// Adds an eventlistener to the main menu to affect the page
document.addEventListener('DOMContentLoaded', function () {
  document.getElementById('themeSelect').addEventListener('change', function () {
    var themeList = document.getElementById('themeSelect')
    var theme = themeList.options[themeList.selectedIndex].value

    chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
      chrome.tabs.sendMessage(tabs[0].id, {theme: theme});
    });
  });
});


// Event listeners for the activate and deactivate buttons
document.getElementById('activate').addEventListener('click', activateScriptonia);
document.getElementById('deactivate').addEventListener('click', deactivateScriptonia);

// Populate the auto activation checkbox if cache has it stored as such
chrome.storage.local.get(['autoActivateOn'], function(results) {
  if (results.autoActivateOn) {
    document.getElementById('auto-activate-option').checked = "true"
  } 
})

// update cache if auto activate option is checked
document.getElementById('auto-activate-option').addEventListener( 'change', function() {
  chrome.storage.local.set({autoActivateOn: this.checked})
})

// toggle line wrapping when checkbox is clicked
document.getElementById("line-wrap-option").addEventListener( 'change', function() {
  var shouldWrap = this.checked

  console.log('saving line wrap options: ' + shouldWrap)
  chrome.storage.local.set({savedLineWrap: shouldWrap})

  chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
    chrome.tabs.executeScript(tabs[0].id, {code: `createSetOptionTempScript("lineWrapping", ${shouldWrap})`});
  });
});

chrome.storage.local.get(['savedLineWrap'], function(result) {
  if (result.savedLineWrap) {
    document.getElementById('line-wrap-option').checked = "true"
  }
})

// Set the value of the the theme select drop down with the cached theme
chrome.storage.local.get(['savedTheme'], function(result) {
  if (result.savedTheme) {
    document.getElementById('themeSelect').value = result.savedTheme
  }
});

chrome.runtime.onMessage.addListener( function (request, sender, sendResponse) {
  setThemeColor( request.theme )
});

// Set the value of the font size with cached font size
chrome.storage.local.get(['savedEditorFontSize'], function(result) {
  if (result.savedEditorFontSize) {
    document.getElementById('editor-font-size').value = result.savedEditorFontSize
  }
});

// toggle font size when dropdown is changed
document.getElementById('editor-font-size').addEventListener( 'change', function() {
  var size = this.options[this.selectedIndex].value

  chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
    chrome.tabs.executeScript(tabs[0].id, {code: `setEditorFontSize(${ size })`})
  })
})

chrome.runtime.onMessage.addListener( function (request, sender, sendResponse) {
  setEditorFontSize( request.editorFontSize )
});
