var quickLoginButtons = document.getElementsByClassName('quick-login-button');
var i;

for (i = 0; i < quickLoginButtons.length; i++) {
  quickLoginButtons[i].addEventListener("click", function() {
    var tenant = document.getElementById(`${this.name}-domain`).value

    openLoginPage( this.name, tenant )
  });
}

var quickLoginInputs = document.getElementsByClassName('login-text')

for (i = 0; i < quickLoginInputs.length; i++) {
  quickLoginInputs[i].addEventListener("keyup", function( event ) {

    event.preventDefault();

    if (event.keyCode === 13) {
      openLoginPage( this.name, this.value )
    }
  });
}

function openLoginPage( domain, tenant ) {
  if (tenant != null || tenant != undefined || tenant != "") {
    url = `https://${domain}.webcomserver.com/wpm/index.jsp?tenantName=${tenant}`
  }
  else {
    // It will just go the normal login
    url = `https://${domain}.webcomserver.com/wpm/index.jsp`
  }

  window.open(url)
}
