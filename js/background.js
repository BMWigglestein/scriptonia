function updateStatus( ) {
  chrome.tabs.query( { active: true, currentWindow: true }, function( tabs ) {
    if( tabs[0].url.includes( 'showScripts.do' ) ) {
      chrome.browserAction.setIcon({
        path : {
          "19": "images/Scriptonia19.png",
          "38": "images/Scriptonia38.png",
          "128": "images/Scriptonia128.png"
        }
      });

      chrome.browserAction.setPopup({
          popup: "main_menu.html"
      });

      chrome.storage.local.get(['autoActivateOn'], function(results) {
        if (results.autoActivateOn) {
          setTimeout(function() {
            chrome.tabs.executeScript({
              file: 'js/activate.js'
            });
          }, 6000) 
        } 
      })
    }
    else {
      chrome.browserAction.setIcon({
        path : {
          "19": "images/Scriptonia19_bw.png"
        }
      });

      chrome.browserAction.setPopup({
          popup: "nah_bruh.html"
      });
    }
  });
}

chrome.tabs.onActivated.addListener( function ( activeInfo ) {
  updateStatus()
});

chrome.tabs.onUpdated.addListener( function ( activeInfo ) {
  updateStatus()
});

