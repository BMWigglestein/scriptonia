chrome.storage.local.get(['savedTheme'], function(result) {
  if (result.savedTheme) {
    // console.log("setting theme color on startup: " + result.savedTheme)
    setThemeColor(result.savedTheme)
  }
});

chrome.storage.local.get(['savedEditorFontSize'], function(result) {
  if (result.savedEditorFontSize) {
    console.log("Setting the font size on startup: " + result.savedEditorFontSize)
    setEditorFontSize(result.savedEditorFontSize)
  }
})

chrome.storage.local.get(['savedLineWrap'], function(result) {
  console.log('checking line wrap option:" ' + result.savedLineWrap)
  if (result.savedLineWrap) {
    createSetOptionTempScript("lineWrapping", result.savedLineWrap)
  }
})

chrome.runtime.onMessage.addListener( function (request, sender, sendResponse) {
  setThemeColor( request.theme )
});

//Options that Code Mirror provides
createSetOptionTempScript("tabSize", 2)
createSetOptionTempScript("dragDrop", false)
createSetOptionTempScript("showCursorWhenSelecting", true)
createSetOptionTempScript("dragDrop", false)
createSetOptionTempScript("mode", "text/x-groovy", "output")
addTooltipToTabs()
setLineHeight(18)
setLetterSpacing(.3)
setPageWidth()
fixLongLineBug()
makeContextMenuReadable()
addValidateKeyMap()
addFullScreenKeyMap()
disableScrollOnEditorHover()
bindClick()
destroyWalkmeWidget()
createGlobalVariables()
createGlobalStyles()
updateTabColors()
createFancyTreeListener()
activateGutterFolding() 
createGutterMatchStyles()
addMatchingHighlightGutter()

/*
** Set all of the initial variables that will be used when Scriptonia is 
** activated.
*/
function createGlobalVariables() {
  var scriptGlobals = `
    var cachedCursors = new Map()
    var fileTreeFlag = false
    var contextMenuFlag = false
    var consoleFlag = false
    var editorBgColor
    var editorTextColor
    var editorGutterColor
    /* 
    ** The following are components that are moved around frequently when toggling
    ** full screen, and the menus when full screen is enabled
    */
    // editor components
    var editor = $('#code').next()
    // file tree components
    var scriptIDETree = $('#script-ide-tree')
    var tree = $('#tree')
    var treeUI = $('.ui-fancytree')
    // context menu components
    var scriptIDESectionContext = $('#script-ide-section-context')
    // console components
    var scriptIDESectionViews = $('#script-ide-section-views')
    var scriptIDESectionOutput = $('#script-ide-section-output')
    var scriptIDESectionUnitTests = $('#script-ide-section-unit-tests')
    var scriptIDESectionHistory = $('#script-ide-section-history')
    var baseFullConsole = ($(window).width() - 54)
  `

  createPermScript(scriptGlobals)
  updateCursorCache()
}

function createGlobalStyles() {
  var globalStyles = `
    /* Tab Styles based on script types */
    .ts-1 { box-shadow: 0 6px 0 #0042ff; } /* Workflow */
    .ts-2 { box-shadow: 0 6px 0 #fe00ef; } /* Event */
    .ts-3 { box-shadow: 0 6px 0 #ffc300; } /* Scheduled Task */
    .ts-5 { box-shadow: 0 6px 0 #8d8d8d; } /* Web Action */
    .ts-6 { box-shadow: 0 6px 0 #16e900; } /* Form Event */
    .ts-7 { box-shadow: 0 6px 0 #33dbff; } /* Unit Test */
    .ts-8 { box-shadow: 0 6px 0 #ff0000; } /* Internal User */
    .ts-9 { box-shadow: 0 6px 0 #006a3e; } /* Asynchronous Task */
    .ts-10 { box-shadow: 0 6px 0 #7d00b1; } /* Exception Handling */
    .ts-11 { box-shadow: 0 6px 0 #ef7818; } /* Utils */
  `

  var style = document.createElement('style')
  style.rel = 'stylesheet'
  style.id = 'global-styles'
  style.innerHTML = globalStyles

  document.getElementsByTagName( 'head' )[0].appendChild( style )
}

