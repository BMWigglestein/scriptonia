/*
** Resets all the styles to look like the extension was never activated... All of the functional 
** fixes are still there (as long as the extension was activated before this function is run)
*/
resetEverythingNonFunctional()

function resetEverythingNonFunctional() {
  var editor = document.getElementsByClassName( 'CodeMirror-wrap' )[0]
  editor.style.cssText = "height: 500px"

  document.getElementById( 'script-ide' ).style.cssText = ""

  //todo: go back to default
  setThemeColor( "default" )
}