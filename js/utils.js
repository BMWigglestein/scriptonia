/*
** Sets the height of the editor
**
** @param height: the height to set the editor to
*/
function setEditorHeight( height ) {
  var editor = document.getElementsByClassName( 'CodeMirror-wrap' )[0]
  editor.style.cssText = `height: ${ height }px;`
}

/*
** Sets the line-height of the editor
**
** @param lineHeight: the desired line-height
*/
function setLineHeight( lineHeight ) {
  var editor = document.getElementsByClassName( 'CodeMirror-wrap' )[0]
  editor.style.cssText += `line-height: ${ lineHeight }px;`
}

/*
** Sets the letter-spacing of the editor
**
** @param letterSpacing: the desired letter-spacing
*/
function setLetterSpacing( letterSpacing ) {
  var editor = document.getElementsByClassName( 'CodeMirror-wrap' )[0]
  editor.style.cssText += `letter-spacing: ${ letterSpacing }px;`
}

/*
** Sets the width of the script page to 98%, instead of 90%
*/
function setPageWidth() {
  document.getElementById('script-ide').style.cssText = "min-width: 98%;"
}

/*
** Creates a url of CodeMirror's available theme.
**
** @param themName: the name of the theme that is being loaded
**
** @return the constructed url of the theme's css file
*/
function getThemeHref( themeName ) {
  return `https://codemirror.net/theme/${ themeName }.css`
}

/*
** Fixes the issue where very long strings without spaces force the editor to expand to the size of the string.
** In turn making it literally unplayable
**
** // todo: check for resize of window, if so... then update max-width
*/
function fixLongLineBug() {
  function bugFix() {
    // Getting 79% of the script ide element
    var maxWidth = document.getElementById( 'script-ide' ).offsetWidth * .79

    // Set the max width of the script ide section to 79% of the script ide element
    var scriptIdeSection = document.getElementById( 'script-ide-section' )
    var existingStyle = scriptIdeSection.style.cssText
    scriptIdeSection.style.cssText = `${ existingStyle } max-width: ${ maxWidth }px;`
  }

  bugFix()

  // todo: make sure this doesn't add a million even listeners when resizing
  window.addEventListener( 'resize', function() {
    bugFix()
  })
}

/*
** Adds some styles to the context menu to make it more pretty
*/
function makeContextMenuReadable() {
  // todo: make the colors for the context menu dynamic with theme
  var contextStyle = `
    .scriptResouceObjectContent div{ border-bottom: 1px solid; font-size: 12px; }
    #objectInfo.classInfo { background-color: initial; }
  `
 
  var style = document.createElement('style')
  style.rel = 'stylesheet'
  style.id = 'context-menu-styles'
  style.innerHTML = contextStyle

  document.getElementsByTagName( 'head' )[0].appendChild( style )
}

/*
** Change the font size of the editor
**
** @param size: the desired size of the editor
*/
function setEditorFontSize( size ) {
  chrome.storage.local.set({savedEditorFontSize: size})
  var editorFontSizeStyle = `
    .CodeMirror { font-size: ${size}px; }
  `

  var style = document.createElement('style')
  style.rel = 'stylesheet'
  style.innerHTML = editorFontSizeStyle

  document.getElementsByTagName( 'head' )[0].appendChild( style )
}

/*
** adds the editor theme to the page
**
** @param themeName: the name of the theme to be loaded
*/
function setThemeColor( themeName ) {
  // Create link element 
  var link = document.createElement( 'link' )
  link.href = getThemeHref( themeName )
  link.rel = 'stylesheet'

  
  // add it to the head of the page
  document.getElementsByTagName( 'head' )[0].appendChild( link )

  createSetOptionTempScript ( "theme", themeName )
  createSetOptionTempScript ( "theme", themeName, 'output' )

  // Timeout because it runs before the css is set in the browser sometimes
  setTimeout(function() {
    fixCursorColor( themeName )

    // Set the colors from the editor
    var scriptContent = `
      editorBgColor = $('.cm-s-${ themeName }.CodeMirror').css('background')
      editorTextColor = $('.cm-s-${ themeName }.CodeMirror').css('color')
      editorGutterColor = $('.cm-s-${ themeName } .CodeMirror-gutters').css('background')
    `
    createTempScript( scriptContent )

    chrome.storage.local.set({savedTheme: themeName})
  }, 1000)
}

/*
** Sometimes, the cursor color wouldn't update.  This fixes that
**
** @param themeName: the name of the theme to be loaded
*/
function fixCursorColor( themeName ) {
  var scriptContent =`
    editorTextColor = $('.cm-s-${ themeName }.CodeMirror').css('color')
    var cursorfix = document.createElement("style")

    cursorfix.type = "text/css"
    cursorfix.innerText = ".CodeMirror-cursor {border-color: " + editorTextColor + " !important}"

    document.getElementsByTagName( 'head' )[0].appendChild( cursorfix )
  `

  createTempScript( scriptContent )
}

/*
** Adds a border radius to script selection tabs.
** This is not being used ** 
*/
function addBorderRadiusToTabs() {
  var scriptContent = `
    var roundStyle = document.createElement("style")

    roundStyle.type = "text/css"
    roundStyle.innerText = "#script-ide-tabs .tab {border-radius: 7px 7px 0 0;}"

    document.getElementsByTagName( 'head' )[0].appendChild( roundStyle )
  `

  createTempScript( scriptContent )
}

/*
** Disable the page scrolling when the cursor is over the editor
*/
function disableScrollOnEditorHover() {
  var scriptContent = `
    $.fn.disableScroll = function() {
      window.oldScrollPos = $(window).scrollTop();

      $(window).on('scroll.scrolldisabler',function ( event ) {
        $(window).scrollTop( window.oldScrollPos );
        event.preventDefault();
      });
  };

  $.fn.enableScroll = function() {
    $(window).off('scroll.scrolldisabler');
  };

  var editor = $('#script-ide-section-editor');

  editor.hover( function() {
    $('body').disableScroll()
  }, function() {
    $('body').enableScroll()
  });
  `

  var styleContent = `
    var minHeight = document.createElement("style")

    minHeight.type = "text/css"
    minHeight.innerText = ".CodeMirror-sizer {min-height: 501px !important;}"

    document.getElementsByTagName( 'head' )[0].appendChild( minHeight)
  `

  createTempScript( styleContent )
  createPermScript( scriptContent )
}

/*
** Adds the ability to toggle between fullscreen mode with F11.  This shortcut also 
** manipulates the output box to show during full screen
*/
function addFullScreenKeyMap() {
  var scriptContent = `
    var map = {"F11": function() {
      code.setOption("fullScreen", !code.getOption("fullScreen"))

      if (code.options.fullScreen) {
        ${ getFullScreenStyles() }
      }
      else {
        ${ getDefaultStyles() }
      }
    }}

    code.addKeyMap(map)
  `
  createTempScript( scriptContent )

  scriptContent = `
    var map = {"Esc": function() {
      if (code.options.fullScreen) {
        code.setOption("fullScreen", !code.getOption("fullScreen"))

        ${getDefaultStyles()}
      }
    }}

    code.addKeyMap(map)
  `

  createTempScript( scriptContent )

  engageFullscreenIDE()
}

/*
** Adds the menu elements that you see when you initiate full screen mode
*/
function addFullscreenElements() {
  var div = document.createElement('div')
  div.id = 'menu-items'

  div.innerHTML = `
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
  <div id="icons">
    <div id="file-tree" class="icon" title="File Tree"><p><i class="fa fa-folder fa-3x" aria-hidden="true"></i></p></div>
    <div id="context-menu" class="icon" title="Context Menu"><p><i class="fa fa-compass fa-3x" aria-hidden="true"></i></p></div>
    <div id="console" class="icon" title="Output Console"><p><i class="fa fa-terminal fa-3x" aria-hidden="true"></i></p></div>
  </div>
  <div id="info">
  </div>
  `

  document.body.appendChild(div)
}

/*
** Apparently just adds a bunch of styles...
** This is poorly named.  Not really sure what happened here :(
*/ 
function addFullscreenScript() {
  var style = document.createElement('style')

  style.type = 'text/css'
  style.innerText = `
    #menu-items {
      display: none;
      position: fixed;
      top: 0;
      left: 0;
      width: 70px;
      height: 100%;

      z-index: 999;
      background: gray; /* will be the background color of the editor */
    }
    #icons {
      position: relative;
      top: 32px;
    }
    .icon {
      width: 60px;
      height: 60px;
      margin-top: 5px;
      margin-left: auto;
      margin-right: auto;

      text-align: center;
    }
    /* will be the color of the text editor, and probably an icon */
    .icon p { font-weight: bold; user-select: none; }
    .icon img {
      width: 100%;
      height: 100%;
    }
    .icon:hover {
      cursor: pointer;
    }
  ` 

  // add it to the head of the page
  document.getElementsByTagName( 'head' )[0].appendChild( style )
}

/*
 * Add styles for the full screen IDE stuff
 * 
 */
function getFullScreenStyles() {
  var styles = `
    var menu = $('#menu-items')
    menu.css('display', 'block')
    menu.css('background', editorBgColor)
    menu.css('color', editorTextColor)

    editor.css('left', 70)
    editor.css('top', 30)
    editor.css('line-height', '18px')
    editor.css('padding-top', '6px')

    // tabs
    var tabs = $('#script-ide-tabs')
    tabs.css('position', "fixed")
    tabs.css('top', 0)
    tabs.css('left', 70)
    tabs.css('z-index', 20)
    tabs.css('width', '96.7%')
    tabs.css('height', '38')
    tabs.css('background', editorGutterColor) // will be the background color of the theme
  `

  return styles
}

/*
** Default styles are the styles that display when deactivate is pressed
** // todo: update the default styles...
**
** @ return: the aforementioned styles
*/
function getDefaultStyles() {
  var styles = `
    var menu = $('#menu-items').css('display', 'none')

    editor.attr("style", 'height: 500px; line-height: 18px;')

    var tabs = $('#script-ide-tabs')
    tabs.attr("style", '')

    scriptIDETree.attr('style', '')
    tree.attr('style', 'height: 950px; overflow: auto;')
    scriptIDESectionContext.attr('style', '')
    scriptIDESectionViews.attr('style', '')
    scriptIDESectionOutput.attr('style', '')
    scriptIDESectionUnitTests.attr('style', 'display: none')
    scriptIDESectionHistory.attr('style', 'display: none')

    fileTreeFlag = false
    contextMenuFlag = false
    consoleFlage = false
  `

  return styles
}

/*
** Adds the necessary style for the full screen IDE functionality
*/
function addFullscreenStyle() {
  var scriptContent = `
    // header

    // console tabs

    // console

    // menu
    var fileTreeIcon = $('#file-tree')
    var contextIcon = $('#context-menu')
    var consoleIcon = $('#console')

    fileTreeIcon.click(function() {
      ${getToggleFileTreeCode()}
    }) 
    contextIcon.click( function() {
    ${getToggleContextCode()}
    })
    consoleIcon.click( function() {
      ${getToggleConsoleCode()}
    })
  `

  createPermScript( scriptContent )
}

/*
** Created necessary code to toggle the file tree
**
** @ return: The aformentioned code
*/
function getToggleFileTreeCode() {

  var scriptContent = `
    if (!fileTreeFlag) { // file tree is currently closed -> open it
      scriptIDETree.attr('style', "display: block; position: fixed; top: 33px; left: 70px; background: white; z-index: 50; border: none; background: " + editorBgColor + "; height: 100%")
      scriptIDETree.find('div')[0].style.backgroundColor = editorBgColor.split('none')[0]
      tree.attr('style', 'height: 92.5%; overflow: auto;');
      treeUI.attr('style', 'background: ' + editorBgColor + ';')

      $("style").append(".fancytree-title {color: " + editorTextColor + " !important;}")

      var treeWidth = scriptIDETree.width()

      editor.css('left', (treeWidth + 70) + "px")
      editor.css('height', (editor.height()) + "px")

      scriptIDESectionViews.css('left', (treeWidth + 70) + "px")
      scriptIDESectionViews.css('width', scriptIDESectionViews.width() - treeWidth)

      scriptIDESectionOutput.css('left', (treeWidth + 69) + "px")
      scriptIDESectionOutput.css('width', scriptIDESectionOutput.width() - treeWidth)

      scriptIDESectionUnitTests.css('left', (treeWidth + 69) + "px")
      scriptIDESectionUnitTests.css('width', scriptIDESectionUnitTests.width() - treeWidth)

      scriptIDESectionHistory.css('left', (treeWidth + 69) + "px")
      scriptIDESectionHistory.css('width', (scriptIDESectionHistory.width() - treeWidth) + "px")
    }
    else { // file tree is open -> close it
      scriptIDETree.attr('style', 'display: block;')
      
      editor.css('left', "70px")

      scriptIDESectionViews.css('left', "70px")
      scriptIDESectionViews.css('width', baseFullConsole + "px")

      scriptIDESectionOutput.css('left', "69px")
      scriptIDESectionOutput.css('width', baseFullConsole + "px")

      scriptIDESectionUnitTests.css('left', "69px")
      scriptIDESectionUnitTests.css('width', baseFullConsole + "px")

      scriptIDESectionHistory.css('left', "69px")
      scriptIDESectionHistory.css('width', baseFullConsole + "px")
    }

    fileTreeFlag = !fileTreeFlag
  `

  return scriptContent
}

/*
** Created necessary code to toggle the context menu
**
** @ return: The aformentioned code
*/
function getToggleContextCode() {
  var scriptContent = `
    if (!contextMenuFlag) {
      scriptIDESectionContext.attr('style', "position: fixed; top: 31px; right: 0; padding: 0; margin: 0; background: " + editorBgColor + "; color: " + editorTextColor + "; z-index: 50;")
    }
    else {
      scriptIDESectionContext.attr('style', '')
    }

    contextMenuFlag = !contextMenuFlag
  `

  return scriptContent
}

/*
** Created necessary code to toggle the output console
**
** @ return: The aformentioned code
*/
function getToggleConsoleCode() {
  var onScript = `
    var tree = $('#script-ide-tree') 
    var leftPos = 70
    var consoleWidth = $(window).width() - 71

    if (fileTreeFlag) {
      leftPos = leftPos + tree.width()
      consoleWidth = consoleWidth - scriptIDETree.width()
    }

    var viewStyles = 'position: fixed; bottom : ' + (scriptIDESectionOutput.height() + scriptIDESectionViews.height() - 44) + 'px; left: ' + (leftPos) + 'px; background: ' + editorGutterColor + '; z-index: 65; width: ' + consoleWidth + 'px; height: 32px;'

    var consoleStyles = "position: fixed; bottom: 0; left: " + (leftPos - 1) + "px; padding: 0; margin: 0; background: white; width:" + consoleWidth + "px; border-color: none;"

    scriptIDESectionViews.attr('style', viewStyles)
    scriptIDESectionOutput.attr('style', consoleStyles + "z-index: 60;")
    scriptIDESectionUnitTests.attr('style', consoleStyles + "z-index: 55;")
    scriptIDESectionHistory.attr('style', consoleStyles + "z-index: 50;")

    editor.css('height', ((editor.innerHeight() - scriptIDESectionOutput.innerHeight()) - 18) + "px")
  `

  var offScript = `
    scriptIDESectionViews.attr('style', '')
    scriptIDESectionOutput.attr('style', '')
    scriptIDESectionUnitTests.attr('style', 'display: none')
    scriptIDESectionHistory.attr('style', 'display: none')

    editor.css('height', '100%')
    editor.height(editor.height() - 30)
  `

  var scriptContent = `
    if (!consoleFlag) {
      ${onScript}
    }
    else {
      ${offScript}
    }

    consoleFlag = !consoleFlag
  `

  return scriptContent
}

/*
** Starts the process that builds the full screen IDE stuff
*/
function engageFullscreenIDE() {
  addFullscreenElements()
  addFullscreenStyle()
  addFullscreenScript()
}

/*
** Add the ability to validate script with Ctrl+; shortcut.
*/
function addValidateKeyMap() {
  var scriptContent = `
    var map = { "Ctrl-;": function() {
      scriptIDE.validateCode()
    }}
    code.addKeyMap(map)
  `

  createTempScript( scriptContent )
}

/*
** Add the ability to scroll to the text editor with Ctrl+' shortcut.
** This is not being used
*/
function addScrollToEditorKeyMap() {
  var scriptContent = `
    var map = { "Ctrl-'": function() {
      var cms = document.getElementsByClassName('CodeMirror-cursors')
      cms[0].scrollIntoView({behavior: "smooth"})
    }}
    code.addKeyMap(map)
  `

  createTempScript( scriptContent )
}

function getTabColorsCode() {
  return `
    var allTabs = $('#script-ide-tabs div.tab')
    var tsClass, scriptId

    for (var i = 0; i < allTabs.length - 1; i++) {
      scriptId = allTabs[i].getAttribute('scriptid').split('#')[0]
      
      var associatedScript = loadedScripts.find(function(script) {
        return script.id == scriptId
      })

      tsClass = "ts-" + associatedScript.type

      if (!allTabs[i].className.includes('tabMore') && !allTabs[i].className.includes(tsClass)) {
        allTabs[i].className += " " + tsClass
      }
    }
  `
}

function updateTabColors() {
  createTempScript( getTabColorsCode() )
}

function getTabTooltipCode() {
  return `
    var allTabs = $('#script-ide-tabs div.tab')
    var title, scriptId

    for (var i = 0; i < allTabs.length - 1; i++) {
      scriptId = allTabs[i].getAttribute('scriptid').split('#')[0]

      var associatedScript = loadedScripts.find(function(script) {
        return script.id == scriptId
      })

    allTabs[i].setAttribute('title', associatedScript.name)
    }
  `
}
/*
** Adds a hover over tooltip to each tab that says the name of the script
*/
function addTooltipToTabs() {
  createTempScript( getTabTooltipCode() )
}

/*
** Sets the position of the cursor based on the previously saved position
**
** @param clickedScriptId: this is the key of the map that holds the previous positions
*/
function setCursorPosition ( clickedScriptId ) {
  var scriptContent = `
    var tab = cachedCursors.get("${clickedScriptId}")

    if (tab) {
      var x = parseInt(tab.split("#")[0])
      var y = parseInt(tab.split("#")[1]) + 1

      setTimeout(function() {
        code.focus()
        code.setCursor({line: x, ch: y})
        code.scrollIntoView({line: x, char: y}, 250) // 250 should be hald editor size... 
      }, 150)
    }
  `

  createTempScript( scriptContent )
}

/*
** Adds an event listener to check if click or middle click was clicked on a tab.  It 
** middle click was on a tab, then close the tab and the associated editor. If left click 
** was on a tab, set cursor to the cached value
*/
function bindClick() {
  
  function closeTabWithMiddleClick() {
    document.onmousedown = function(e) { 
      var parent = e.target.parentElement

      if (parent) {
        if ( parent.className.includes('tab') || parent.className.includes('tab selected') ) {
          var scriptid = parent.getAttribute( "scriptid" )

          if ( e.which == 2 ) {
            e.preventDefault()
            updateTabColors()
            addTooltipToTabs()
            closeTab( scriptid )
          }
          else if ( e.which == 1 ) {
            setCursorPosition( scriptid.split("#")[0] )
          }
        }
      }
    }
  }

  document.addEventListener( 'click', closeTabWithMiddleClick );
}

/*
** Adds an event listener that checks if a script is double clicked in the file tree
*/
function createFancyTreeListener() {
  var scriptContent = `
    $('.fancytree-has-children').next('ul').dblclick(function() {
      setTimeout(function() {
        ${getTabColorsCode()}
        ${getTabTooltipCode()}
      }, 400)
    })
  `
  
  createPermScript(scriptContent)
}

function createGutterMatchStyles() {
  setTimeout(function() { // timeout needed to insure colors are set by theme
    var styleTagScript = `
      $( "<style id='gutter-match-styles'>.gutter-match { background: " + editorTextColor + "; color: " + editorBgColor.toString().split('none')[0] + " !important; }</style>" ).appendTo( "body" )
    `
    createTempScript(styleTagScript)
  }, 1100)
}

function updateGutterMatchStyles() {
  setTimeout(function() {
    var scriptContent = `
      $('#gutter-match-styles').innerText(".gutter-match { background: " + editorTextColor + "; color: " + editorBgColor.toString().split('none')[0] + " !important; }")
    `
    createTempScript(scriptContent)
  }, 1100)
}

/*
** Adds an event listener that fires whenever the cursor is moved in the editor
** When the event fires, it highlights the gutter where a matching word is
*/
function addMatchingHighlightGutter() {
  var scriptContent = `
    code.on('cursorActivity', function() {
      setTimeout(function() {
        var matchingWords = $('.cm-matchhighlight')
        var gutterEl
 
       matchingWords.each(function() {
         gutterEl = $(this).closest('div').find('.CodeMirror-linenumber')
         gutterEl.addClass('gutter-match')
       })
      }, 200)
    })

    code.on('scroll', function() {
      setTimeout(function() {
        var matchingWords = $('.cm-matchhighlight')
        var gutterEl
 
       matchingWords.each(function() {
         gutterEl = $(this).closest('div').find('.CodeMirror-linenumber')
         gutterEl.addClass('gutter-match')
       })
      }, 200)
    })
  `
  
  createPermScript(scriptContent)
}

/*
** Saves the position of the cursor for each tab when a tab is clicked
*/
function updateCursorCache() {
  var scriptContent = `
    $('.CodeMirror.CodeMirror-wrap').keydown(function(event) {
      var scriptid = $('.tab.selected').attr('scriptid').split('#')[0]
      cachedCursors.set(scriptid, scriptIDE.getCursorPositionCoordinates())
    });
    $('.CodeMirror.CodeMirror-wrap').click(function() {
      var scriptid = $('.tab.selected').attr('scriptid').split('#')[0]
      cachedCursors.set(scriptid, scriptIDE.getCursorPositionCoordinates())
    })
  `

  createTempScript( scriptContent )
}
  
/*
** Closes an open tab
**
** @param scriptid: a script it in the form of XXX or XXX#YYY, where the latter signifies that 
** the open script has a draft associated with it
*/
function closeTab( scriptid ) {
  // Split the script id based on the '#' character
  var scriptids = scriptid.split( "#" )

  var scriptContent = ``
  // add script content that calls a close tab with or without a draft id associated with it
  if ( scriptids.length > 1 ) {
    // has draft id
    scriptContent = `closeTab('${scriptids[0]}', true, '${scriptids[scriptids.length - 1]}');`
  }
  else {
    // no draft id
    scriptContent = `closeTab('${scriptid}');`
  }

  scriptContent += `
    if (loadedScripts.length < 1) {
      code.setOption("fullScreen", false)

      ${ getDefaultStyles() }
    }
  `

  createTempScript( scriptContent )
} 


function activateGutterFolding() {
  addFoldStyles()
  addFoldScripts()
  createTempScript(`code.setOption('gutters', ["CodeMirror-linenumbers", "CodeMirror-foldgutter"])`)
  createTempScript(`code.setOption('foldOptions', null)`)

  setTimeout(function() {
    createTempScript(`code.setOption('foldGutter', { rangeFinder: new CodeMirror.fold.combine(CodeMirror.fold.indent, CodeMirror.fold.comment) } )`)
  }, 500)

  setTimeout(function() {
    createTempScript(`code.refresh()`)  
  }, 700)
}

function addFoldStyles() {
    // Create link element 
    var link = document.createElement( 'link' )
    link.href = "https://codemirror.net/addon/fold/foldgutter.css" 
    link.rel = 'stylesheet'
    
    // add it to the head of the page
    document.getElementsByTagName( 'head' )[0].appendChild( link )
}

function addFoldScripts() {
  var urls = [
    "https://codemirror.net/addon/fold/foldcode.js",
    "https://codemirror.net/addon/fold/foldgutter.js",
    "https://codemirror.net/addon/fold/brace-fold.js",
    "https://codemirror.net/addon/fold/indent-fold.js",
    "https://codemirror.net/addon/fold/comment-fold.js",
  ]

  for (var i = 0; i < urls.length; i++) {
    var script = document.createElement( 'script' )
    script.src = urls[i]
  
    console.log(script)
    document.getElementsByTagName( 'head' )[0].appendChild( script )
  }
}

/*
** creates a script element, adds the content, then addis it to the dom
** this script element is not deleted
**
**
 @param scriptContent: a string of text that represents the innerText of the script element
*/ 
function createPermScript( scriptContent ) {
  var script = document.createElement( 'script' )
  script.type = 'text/javascript'
  script.id = "permScript"

  script.text = scriptContent

  // Add script content to script and append it to the head
  document.getElementsByTagName( 'head' )[0].appendChild( script )
}

/*
** Creates a script element, adds the content, then adds it to the dom, then deletes it
**
** @param scriptContent: a string of text that represents the innerText of the script element
*/
function createTempScript( scriptContent ) {
  var script = document.createElement( 'script' )
  script.id = 'tempScript'

  // Add script content to script and append it to the body
  script.appendChild( document.createTextNode( scriptContent) ) 
  document.body.appendChild( script )

  // Remove the script
  let temp = document.getElementById( 'tempScript' )
   temp.parentNode.removeChild( temp )
}

/*
** creates the scriptContent to be passed to the createTempScript function
**
** @param option: the name of the option to be changed
** @param value: the new value of the option to be changed
*/
function createSetOptionTempScript( option, value, editor = 'code' ) {
  if (typeof value === 'string' || value instanceof String) {
    value = `'${value}'`
  }
  
  var scriptContent = `${ editor }.setOption('${ option }', ${ value })`

  createTempScript( scriptContent )
}

/*
** Destroys the very unhelpful widget at the bottom of the screen.
*/
function destroyWalkmeWidget() {
  var scriptContent = `_walkMe.destroyAll()`

  createTempScript( scriptContent )
}